package com.davidMontaguano.muebles;

import com.davidMontaguano.muebles.base.Mueble;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;


public class Vista {

    /**
     * Declaramos las variables para pintar la ventana y posteriormente rellenarla de diferentes
     * campos utiles para la introduccion de los datos de los productos de la tienda de Mueble.
     */
    private JFrame frame;
    private JPanel panel1;

    /**
     * En esta parte del codigo almacena los compos que componen la ventana
     * en la cual tenemos:
     * JtextField, JComboBox,JButton,JTextPane
     */
    private JLabel nombreMuebles;
    private JTextField nombreMuebleTxt;
    private JTextField tipoMuebleTxt;
    private JTextField procedenciaTxt;
    private JTextField medidaTxt;
    private JTextField calidadTxt;
    private JTextField precioTxt;
    private JComboBox desplegable;
    private JCheckBox SICheckBox;
    private JCheckBox NOCheckBox;
    private JButton altaMueble;
    private JButton mostrarMueble;
    private JTextPane textMueble;
    private JButton botonCerrar;

    private JComboBox comboBox;

    private LinkedList<Mueble> lista;
    private DefaultComboBoxModel<Mueble> dcbm;

    public Vista() {
        frame = new JFrame("Tienda de Muebles");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

        crearMenu();
        frame.setLocationRelativeTo(null);
        lista = new LinkedList<>();
        dcbm = new DefaultComboBoxModel<>();
        desplegable.setModel(dcbm);

        /**
         *En estos metodos nos encargamos de dar funcionalidad
         * a los botones
         */

        /**
         * Este metodo se encarga de dar funcionalidad al boton Alta mueble
         */
        altaMueble.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                altaMueble(nombreMuebleTxt.getText(),tipoMuebleTxt.getText(),procedenciaTxt.getText(),medidaTxt.getText(),precioTxt.getText(),calidadTxt.getText());
                refrescarComboBox();
            }
        });
        /**
         * Este metodo se encarga de dar funcionalidad al boton Mostrar Mueble
         */
        mostrarMueble.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Mueble seleccionado = (Mueble) dcbm.getSelectedItem();
                textMueble.setText(seleccionado.toString());
            }
        });

        /**
         * Este metodo se encarga de cerrar la ventanda con el uso del boton Finalizar Vista Datos
         * y salir de la ventana de introducir dato cuando el cliente lo requiera
         */
        botonCerrar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                System.exit(0);
            }
        });

    }
    private void refrescarComboBox() {
        dcbm.removeAllElements();
        for (Mueble mueble: lista) {
            dcbm.addElement(mueble);
        }
    }

    public static void main(String[] args) {

        Vista vista = new Vista();
    }

    /**
     *  Este metodo se encarga de añadir un nuevo mueble con sus respectivos campos
     * @param nombre
     * @param tipoMueble
     * @param procedencia
     * @param medida
     * @param precio
     * @param calidad
     */
    private void altaMueble(String nombre, String tipoMueble, String procedencia, String medida, String precio, String calidad) {
        lista.add(new Mueble(nombre, tipoMueble, procedencia, medida, precio, calidad));
    }

    /**
     * Este metodo se encarga de la creacion del menu Archivo, en el cual puedes exportar o importar los datos
     * de la tabla
     */
    private void crearMenu() {
        JMenuBar barra= new JMenuBar();
        JMenu menu= new JMenu("Archivo");
        JMenuItem itemExportarXML= new JMenuItem("Exportar XML");
        JMenuItem itemImportarXML= new JMenuItem("Importar XML");

        itemExportarXML.addActionListener(new ActionListener() {

            /**
             * Damos funcionalidad al boton Exportar xml
             * @param e
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser selectorArchivo = new JFileChooser();
                int opcionSeleccionada = selectorArchivo.showSaveDialog(null);
                if (opcionSeleccionada == JFileChooser.APPROVE_OPTION) {
                    File fichero = selectorArchivo.getSelectedFile();
                    exportarXML(fichero);
                }

            }
        });

        itemImportarXML.addActionListener(new ActionListener() {
            /**
             * Damos funcionalidad al boton Importar xml
             * @param e
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser selectorArchivo = new JFileChooser();
                int opcion = selectorArchivo.showOpenDialog(null);
                if (opcion == JFileChooser.APPROVE_OPTION) {
                    File fichero = selectorArchivo.getSelectedFile();
                    importarXML(fichero);
                    refrescarComboBox();
                }
            }
        });

        menu.add(itemExportarXML);
        menu.add(itemImportarXML);

        barra.add(menu);
        frame.setJMenuBar(barra);
    }

    /**
     * Este metodo se encarga de importar ficheros para su posterior visualizacion
     * @param fichero
     */
    private void importarXML(File fichero) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document documento = builder.parse(fichero);

            NodeList muebles = documento.getElementsByTagName("mueble");
            for (int i = 0; i < muebles.getLength(); i++) {
                Node mueble= muebles.item(i);
                Element elemento = (Element) mueble;

                String nombre = elemento.getElementsByTagName("nombre").item(0).getChildNodes().item(0).getNodeValue();
                String tipoMueble = elemento.getElementsByTagName("tipoMueble").item(0).getChildNodes().item(0).getNodeValue();
                String procedencia = elemento.getElementsByTagName("procedencia").item(0).getChildNodes().item(0).getNodeValue();
                String medida = elemento.getElementsByTagName("medida").item(0).getChildNodes().item(0).getNodeValue();
                String precio = elemento.getElementsByTagName("precio").item(0).getChildNodes().item(0).getNodeValue();
                String calidad = elemento.getElementsByTagName("calidad").item(0).getChildNodes().item(0).getNodeValue();

                altaMueble(nombre,tipoMueble,procedencia,medida,precio,calidad);
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
    }

    /**
     * Este metodo se encarga de exportar los ficheros
     * @param fichero
     */
    private void exportarXML(File fichero) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        try {
            builder = factory.newDocumentBuilder();
            DOMImplementation dom = builder.getDOMImplementation();

            Document documento= dom.createDocument(null, "xml", null);

            Element raiz= documento.createElement("mueble");
            documento.getDocumentElement().appendChild(raiz);

            Element nodoMueble;
            Element nodoDatos;
            Text dato;

            for (Mueble mueble: lista) {
                nodoMueble= documento.createElement("mueble");
                raiz.appendChild(nodoMueble);

                nodoDatos= documento.createElement("nombre");
                nodoMueble.appendChild(nodoDatos);

                dato= documento.createTextNode(mueble.getNombre());
                nodoDatos.appendChild(dato);

                nodoDatos= documento.createElement("tipoMueble");
                nodoMueble.appendChild(nodoDatos);

                dato= documento.createTextNode(mueble.getTipoMueble());
                nodoDatos.appendChild(dato);




                nodoDatos= documento.createElement("procedencia");
                nodoMueble.appendChild(nodoDatos);

                dato= documento.createTextNode(mueble.getProcedencia());
                nodoDatos.appendChild(dato);



                nodoDatos= documento.createElement("medida");
                nodoMueble.appendChild(nodoDatos);

                dato= documento.createTextNode(mueble.getMedida());
                nodoDatos.appendChild(dato);



                nodoDatos= documento.createElement("precio");
                nodoMueble.appendChild(nodoDatos);

                dato= documento.createTextNode(String.valueOf(mueble.getPrecio()));
                nodoDatos.appendChild(dato);



                nodoDatos= documento.createElement("calidad");
                nodoMueble.appendChild(nodoDatos);

                dato= documento.createTextNode(String.valueOf(mueble.getCalidad()));
                nodoDatos.appendChild(dato);
            }

            Source src = new DOMSource(documento);
            Result result = new StreamResult(fichero);

            Transformer transformer = null;
            transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(src, result);

        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }
}
