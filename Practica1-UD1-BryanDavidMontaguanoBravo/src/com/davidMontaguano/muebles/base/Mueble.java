package com.davidMontaguano.muebles.base;

public class Mueble {
    /**
     * Declaramos las variables del proyecto Tienda de Muebles
     * Todas las varibles son tipo String las cuales son: nombre, tipoMueble
     * procendencia del producto, medida, precio, calidad del producto.
     */
    String nombre;
    String tipoMueble;
    String procedencia;
    String medida;
    String precio;
    String calidad;

    public Mueble(String nombre, String tipoMueble, String procedencia, String medida, String precio,String calidad) {
        this.nombre=nombre;
        this.tipoMueble=tipoMueble;
        this.procedencia=procedencia;
        this.medida=medida;
        this.precio=precio;
        this.calidad=calidad;
    }

    /**
     * Procedemos a la creacion de los getter y setter necesarios para la correcta funcinalidad del proyecto
     * @return
     */
    /**
     * Este metodo se encarda del Getter y Setter de Nombre
     * @return
     */
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Estos Getter y Setter se encargan de devolver datos de la procedencia del producto
     * @return
     */

    public String getProcedencia() {
        return procedencia;
    }

    public void setProcedencia(String procedencia) {
        this.procedencia = procedencia;
    }

    /**
     * Otros dos metodos mas Getter y Setter del campo tipoMueble
     * @return
     */

    public String getTipoMueble() {
        return tipoMueble;
    }

    public void setTipoMueble(String tipoMueble) {
        this.tipoMueble = tipoMueble;
    }

    /**
     * Getter y Setter del campo Medida del producto
     * @return
     */
    public String getMedida() {
        return medida;
    }

    public void setMedida(String medida) {
        this.medida = medida;
    }


    /**
     * Getter y Setter del precio del producto  a la hora de almacenarlo o comprarlo
     * @return
     */
    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    /**
     * Estos dos metodos se encargan de devolver o almacenar la calidad de los productos de la tienda
     * @return
     */
    public String getCalidad() {
        return calidad;
    }

    public void setCalidad(String calidad) {
        this.calidad = calidad;
    }

    /**
     * Este metodo se encarga de la visualizacion de los datos de los productos en los cuales se
     * incluyen: Nombre, Tipo de Mueble, Procedencia, Medida, Calidad y Precio del producto.
     * @return
     */
    @Override
    public String toString() {
        return "Datos Mueble" +
                "\n Nombre= "+nombre+
                "\n Tipo de Mueble= "+tipoMueble+
                "\n Procedencia= "+procedencia+
                "\n Medida= "+medida+
                "\n Calidad= "+calidad+
                "\n Precio= "+precio
                ;

    }
}
